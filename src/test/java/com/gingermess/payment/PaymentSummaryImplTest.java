package com.gingermess.payment;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for the {@link PaymentSummaryImpl} class.
 */
public class PaymentSummaryImplTest {
  
  @Test
  public void testFrom() {
    final PaymentSummaryImpl a = PaymentSummaryImpl.from(BigDecimal.valueOf(10),
                                                         BigDecimal.valueOf(3.33d),
                                                         BigDecimal.valueOf(3.34d),
                                                         3L);
    assertThat(a)
      .extracting("total","paymentCount","regular","last")
      .containsExactly(BigDecimal.valueOf(10),
                       3L,
                       PaymentImpl.from(BigDecimal.valueOf(3.33d)),
                       PaymentImpl.from(BigDecimal.valueOf(3.34d)));
  }
  
  @Test
  public void testPaymentsMultiple() {
    final PaymentSummaryImpl a = PaymentSummaryImpl.from(BigDecimal.valueOf(10),
                                                         BigDecimal.valueOf(3.33d),
                                                         BigDecimal.valueOf(3.34d),
                                                         3L);
    assertThat(a).isNotNull();
    assertThat(a.payments())
      .hasSize(3)
      .containsExactly(PaymentImpl.from(BigDecimal.valueOf(3.33d)),
                       PaymentImpl.from(BigDecimal.valueOf(3.33d)),
                       PaymentImpl.from(BigDecimal.valueOf(3.34d)));
  }
  
  @Test
  public void testPaymentsSingle() {
    final PaymentSummaryImpl a = PaymentSummaryImpl.from(BigDecimal.valueOf(8.45),
                                                         BigDecimal.valueOf(8.45),
                                                         BigDecimal.valueOf(8.45),
                                                         1L);
    assertThat(a).isNotNull();
    assertThat(a.payments())
      .hasSize(1)
      .containsExactly(PaymentImpl.from(BigDecimal.valueOf(8.45d)));
  }
}
