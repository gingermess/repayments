package com.gingermess.payment;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for the {@link PaymentCalculatorImpl} class.
 */
public class PaymentCalculatorImplTest {
  
  private PaymentCalculatorImpl calc;
  
  @Before
  public void init() {
    calc = new PaymentCalculatorImpl();
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testZeroPaymentCount() {
    calc.getPaymentSummary(BigDecimal.TEN, 0);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testNegativePaymentCount() {
    calc.getPaymentSummary(BigDecimal.TEN, -7);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testTooManyPayments() {
    calc.getPaymentSummary(BigDecimal.valueOf(0.01), 3);
  }
  
  @Test
  public void testNegativeTotal() {
    final PaymentSummary a = calc.getPaymentSummary(BigDecimal.valueOf(-0.1d), 3);
    assertThat(a.getTotal()).isEqualByComparingTo(BigDecimal.valueOf(-0.1d));
    assertThat(a.getPaymentCount()).isEqualTo(3L);
    assertThat(a.getRegularPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(-0.03d)));
    assertThat(a.getLastPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(-0.04d)));
    assertThat(a.payments())
      .containsExactly(PaymentImpl.from(BigDecimal.valueOf(-0.03d)),
                       PaymentImpl.from(BigDecimal.valueOf(-0.03d)),
                       PaymentImpl.from(BigDecimal.valueOf(-0.04d)));
  
    final PaymentSummary b = calc.getPaymentSummary(BigDecimal.valueOf(-7653763.52434d), 5);
    assertThat(b.getTotal()).isEqualByComparingTo(BigDecimal.valueOf(-7653763.52434d));
    assertThat(b.getPaymentCount()).isEqualTo(5L);
    assertThat(b.getRegularPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(-1530752.70d)));
    assertThat(b.getLastPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(-1530752.72434d)));
    assertThat(b.payments())
      .containsExactly(PaymentImpl.from(BigDecimal.valueOf(-1530752.70d)),
                       PaymentImpl.from(BigDecimal.valueOf(-1530752.70d)),
                       PaymentImpl.from(BigDecimal.valueOf(-1530752.70d)),
                       PaymentImpl.from(BigDecimal.valueOf(-1530752.70d)),
                       PaymentImpl.from(BigDecimal.valueOf(-1530752.72434d)));
  }
  
  @Test
  public void testPositiveTotal() {
    final PaymentSummary a = calc.getPaymentSummary(BigDecimal.valueOf(0.45), 2);
    assertThat(a.getTotal()).isEqualByComparingTo(BigDecimal.valueOf(0.45));
    assertThat(a.getPaymentCount()).isEqualTo(2L);
    assertThat(a.getRegularPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(0.22)));
    assertThat(a.getLastPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(0.23)));
    assertThat(a.payments())
      .containsExactly(PaymentImpl.from(BigDecimal.valueOf(0.22)),
                       PaymentImpl.from(BigDecimal.valueOf(0.23)));
  
    final PaymentSummary b = calc.getPaymentSummary(BigDecimal.valueOf(5444333222.00), 10);
    assertThat(b.getTotal()).isEqualByComparingTo(BigDecimal.valueOf(5444333222.00));
    assertThat(b.getPaymentCount()).isEqualTo(10L);
    assertThat(b.getRegularPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(544433322.20)));
    assertThat(b.getLastPayment()).isEqualTo(PaymentImpl.from(BigDecimal.valueOf(544433322.20)));
    assertThat(b.payments()).allMatch(payment -> payment.equals(PaymentImpl.from(BigDecimal.valueOf(544433322.20))));
  }

  @Test
  public void testZeroTotal() {
    final PaymentSummary a = calc.getPaymentSummary(BigDecimal.ZERO, 3);
    assertThat(a.getTotal()).isEqualTo(BigDecimal.ZERO);
    assertThat(a.getRegularPayment()).isEqualTo(PaymentImpl.from(BigDecimal.ZERO));
    assertThat(a.getLastPayment()).isEqualTo(PaymentImpl.from(BigDecimal.ZERO));
    assertThat(a.getPaymentCount()).isEqualTo(3L);
    assertThat(a.payments()).allMatch(payment -> payment.equals(PaymentImpl.from(BigDecimal.ZERO)));
  }
  
}
