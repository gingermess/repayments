package com.gingermess.payment.misc;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link ResultImpl}
 */
public class ResultImplTest {
  
  @Test
  public void testSuccess() {
    final Result<String, String> a = ResultImpl.success("yay!");
    assertThat(a.value()).isPresent().get().isEqualTo("yay!");
    assertThat(a.error()).isNotPresent();
    assertThat(a.isSuccess()).isTrue();
  }
  
  @Test
  public void testError() {
    final Result<String, String> b = ResultImpl.error("boom");
    assertThat(b.error()).isPresent().get().isEqualTo("boom");
    assertThat(b.value()).isNotPresent();
    assertThat(b.isSuccess()).isFalse();
  }
}
