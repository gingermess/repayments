package com.gingermess.payment;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for the {@link PaymentImpl} class.
 */
public class PaymentImplTest {
  @Test
  public void testFrom() {
    final PaymentImpl a = PaymentImpl.from(BigDecimal.valueOf(1234));
    assertThat(a.amount()).isEqualByComparingTo(BigDecimal.valueOf(1234));
    assertThat(a).isEqualTo(new PaymentImpl(BigDecimal.valueOf(1234)));
  }
  
  @Test
  public void testCopy() {
    final PaymentImpl a = PaymentImpl.from(BigDecimal.valueOf(23));
    final PaymentImpl b = a.copy();
    assertThat(a).isEqualTo(b);
    assertThat(b.amount()).isEqualByComparingTo(BigDecimal.valueOf(23));
  }
  
  @Test
  public void testEquals() {
    final PaymentImpl a = PaymentImpl.from(BigDecimal.valueOf(-1530752.70d));
    //b will have an amount of: -1530752.7 - note the lack of trailing zero
    final PaymentImpl b = PaymentImpl.from(BigDecimal.valueOf(-7653763.52434d)
                                                     .divide(BigDecimal.valueOf(5), 2, BigDecimal.ROUND_CEILING));
    assertThat(a).isEqualTo(b);
  }
}
