package com.gingermess.payment;

import java.math.BigDecimal;

/**
 * Defines a payment calculator.
 */
public interface PaymentCalculator {
  /**
   * Produce a {@link PaymentSummary} for a total sum to be paid, spread across a number
   * of payments.
   * @param total the total to be paid
   * @param payments the number of payments
   * @return a {@link PaymentSummary}
   */
  PaymentSummary getPaymentSummary(BigDecimal total, long payments);
}
