package com.gingermess.payment;

import java.math.BigDecimal;

/**
 * Defines a single repayment.
 */
public interface Payment {
  /**
   * The amount for this repayment.
   * @return the amount in this repayment
   */
  BigDecimal amount();
}
