package com.gingermess.payment;

import com.gingermess.payment.misc.Result;
import com.gingermess.payment.misc.ResultImpl;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Command line wrapper around {@link PaymentCalculatorImpl}.
 *
 * Parses argument 0 as a BigDecimal, and argument 1 as a Long.
 *
 * Outputs the regular payment and, if present, the last payment.
 */
public class Main
{
  public static void main(String[] args)
  {
    if (args.length != 2) {
      if (args.length != 0) {
        out("Invalid arguments");
      }
      out(getHelp());
      return;
    }
    //parse the total to pay, printing the error if it fails
    final Result<BigDecimal,String> totalResult = parseTotal(args[0]);
    if (!totalResult.isSuccess() || !totalResult.value().isPresent()) {
      out(totalResult.error().orElseGet(Main::getHelp));
      return;
    }
    //as above but for the payments
    final Result<Long,String> countResult = parseCount(args[1]);
    if (!countResult.isSuccess() || !countResult.value().isPresent()) {
      out(countResult.error().orElseGet(Main::getHelp));
      return;
    }
    final BigDecimal total = totalResult.value().get();
    final long count = countResult.value().get();
    //get the summary
    final PaymentSummary summary = new PaymentCalculatorImpl().getPaymentSummary(total, count);
    //print regular, and if applicable the last, payment
    out(String.format("%-16s%s", "Regular Amount", "£"+summary.getRegularPayment().amount().setScale(2, BigDecimal.ROUND_UNNECESSARY)));
    if (!Objects.equals(summary.getRegularPayment(), summary.getLastPayment())) {
      out(String.format("%-16s%s", "Last Amount", "£"+summary.getLastPayment().amount().setScale(2, BigDecimal.ROUND_UNNECESSARY)));
    }
  }
  
  private static Result<Long,String> parseCount(final String value) {
    try {
      return ResultImpl.success(Long.parseLong(value));
    } catch (NumberFormatException e) {
      return ResultImpl.error("Invalid payment count: "+value+", must be an integer number");
    }
  }
  
  private static Result<BigDecimal,String> parseTotal(final String value) {
    try {
      return ResultImpl.success(new BigDecimal(value));
    } catch (NumberFormatException e) {
      return ResultImpl.error("Invalid total: "+value+", must be a number with no currency symbol, and no thousand separator");
    }
  }
  
  private static String getHelp() {
    return "Payment Calculator v1.0\n" +
           "Usage:\n" +
           String.format("%43s","java -jar <jarfile> <total> <payments>\n") +
           "Where:\n" +
           String.format("%13s%70s","<jarfile>","the name of the executable jar, usually repayments-1.0.jar\n") +
           String.format("%11s%99s","<total>","the numerical total to be paid, must be a number containing 0-9 and . characters only\n") +
           String.format("%14s%78s","<payments>","the number of payments to make, must be an integer greater than zero\n") +
           "Example:\n" +
           String.format("%37s","java -jar repayments-1.0.jar 10 3");
  }
  
  private static void out(final String s) {
    System.out.println(s);
  }
}
