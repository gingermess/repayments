package com.gingermess.payment.misc;

import java.util.Optional;

/**
 * Defines a result of an operation that can be either a successful result type, or an error, but not both.
 *
 * @param <A> the successful result type
 * @param <B> the error result type
 */
public interface Result<A, B> {
  /**
   * Determine if this Result describes a successful result.
   * @return true if this Result contains a successful value, false if it contains an error
   */
  boolean isSuccess();
  
  /**
   * Get the successful value, which may not be present if the success is "no result".
   * @return the successful value as an optional, may be empty
   */
  Optional<A> value();
  
  /**
   * Get the error value, which will not be present if isSuccess() returns true.
   * @return the error value, may be empty
   */
  Optional<B> error();
}
