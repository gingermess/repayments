package com.gingermess.payment.misc;

import java.util.Optional;

/**
 * A simple implementation of a {@link Result} that uses static factory methods to
 * avoid providing both a value and an error.
 */
public class ResultImpl<A, B> implements Result<A, B> {
  private final A value;
  private final B error;
  
  public static <A, B> ResultImpl<A, B> success(final A value) {
    return new ResultImpl<>(value, null);
  }
  
  public static <A, B> ResultImpl<A, B> error(final B error) {
    return new ResultImpl<>(null, error);
  }
  
  private ResultImpl(final A value, final B error) {
    this.value = value;
    this.error = error;
  }
  
  @Override
  public boolean isSuccess() {
    return error == null;
  }
  
  @Override
  public Optional<A> value() {
    return Optional.ofNullable(value);
  }
  
  @Override
  public Optional<B> error() {
    return Optional.ofNullable(error);
  }
  
  
}
