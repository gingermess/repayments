package com.gingermess.payment;

import java.math.BigDecimal;

/**
 * A basic implementation of a repayment calculator.
 */
public class PaymentCalculatorImpl implements PaymentCalculator {
  @Override
  public PaymentSummary getPaymentSummary(final BigDecimal total, final long payments) {
    //payments must be at least 1, we can handle anything else
    if (payments < 1) {
      throw new IllegalArgumentException("Number of payments must be greater than 0");
    }
    if (payments == 1 || total.compareTo(BigDecimal.ZERO) == 0) {
      return PaymentSummaryImpl.from(total, total, total, payments);
    }
    //convert the repayment count to a big decimal so we can divide
    final BigDecimal count = BigDecimal.valueOf(payments);
    final BigDecimal each;
    //negative balances require rounding towards positive infinity
    if (total.compareTo(BigDecimal.ZERO) < 0) {
      each = total.divide(count, 2, BigDecimal.ROUND_CEILING);
    } else {
      each = total.divide(count, 2, BigDecimal.ROUND_FLOOR);
    }
    //the last payment is just the normal repayment plus the remainder
    final BigDecimal totalRegular = each.multiply(count);
    if (totalRegular.abs().compareTo(BigDecimal.valueOf(0.01)) < 0) {
      throw new IllegalArgumentException("Too many payments to sensibly split: "+payments);
    }
    final BigDecimal last = each.add(total.remainder(totalRegular));
    return PaymentSummaryImpl.from(total, each, last, payments);
  }
}
