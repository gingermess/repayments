package com.gingermess.payment;

import java.math.BigDecimal;
import java.util.stream.Stream;

/**
 * A basic, immutable, {@link PaymentSummaryImpl}.
 */
public class PaymentSummaryImpl implements PaymentSummary {
  private final BigDecimal total;
  private final long paymentCount;
  private final PaymentImpl regular;
  private final PaymentImpl last;
  
  public PaymentSummaryImpl(final BigDecimal total, final long paymentCount, final  PaymentImpl regular, final PaymentImpl last) {
    this.total = total;
    this.paymentCount = paymentCount;
    this.regular = regular;
    this.last = last;
  }
  
  @Override
  public BigDecimal getTotal() {
    return total;
  }
  
  @Override
  public long getPaymentCount() {
    return paymentCount;
  }
  
  @Override
  public Payment getRegularPayment() {
    return regular;
  }
  
  @Override
  public Payment getLastPayment() {
    return last;
  }
  
  @Override
  public Stream<Payment> payments() {
    if (getRegularPayment().amount().compareTo(getLastPayment().amount()) == 0) {
      return Stream.<Payment>generate(regular::copy).limit(paymentCount);
    } else {
      //concat the last payment to all the others
      return Stream.concat(Stream.generate(regular::copy).limit(paymentCount - 1),
                           Stream.of(last.copy()));
    }
  }
  
  /**
   * Convenience method to create a {@link PaymentSummaryImpl} using just {@link BigDecimal} amounts.
   *
   * @param total the total being paid
   * @param regularPayment the amount of a regular payment
   * @param lastPayment the amount of the last payment
   * @param paymentCount the number of payments, including the last
   * @return a {@link PaymentSummaryImpl} describing the above
   */
  public static PaymentSummaryImpl from(final BigDecimal total,
                                        final BigDecimal regularPayment,
                                        final BigDecimal lastPayment,
                                        final long paymentCount) {
    return new PaymentSummaryImpl(total, paymentCount, PaymentImpl.from(regularPayment), PaymentImpl.from(lastPayment));
  }
}
