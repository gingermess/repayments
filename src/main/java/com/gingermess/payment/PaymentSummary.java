package com.gingermess.payment;

import java.math.BigDecimal;
import java.util.stream.Stream;

/**
 * A summary of a getTotal payment spread over a number of payments.
 */
public interface PaymentSummary {
  /**
   * The total being paid.
   * @return the payment total.
   */
  BigDecimal getTotal();
  
  /**
   * The number of payments.
   * @return the number of payments
   */
  long getPaymentCount();
  
  /**
   * The regular {@link Payment}.
   * @return the regular payment
   */
  Payment getRegularPayment();
  
  /**
   * The last {@link Payment}, which may be the same as the regular.
   * @return the last payment
   */
  Payment getLastPayment();
  
  /**
   * Get a {@link Stream} of {@link Payment}s including the last payment if
   * it's different.
   *
   * @return a stream of all payments
   */
  Stream<Payment> payments();
}
