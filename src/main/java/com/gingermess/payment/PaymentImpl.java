package com.gingermess.payment;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * A basic, immutable, {@link Payment}.
 */
public class PaymentImpl implements Payment {
  private final BigDecimal amount;
  
  /**
   * Create a new {@link PaymentImpl} for the provided amount.
   * @param amount the amount
   */
  public PaymentImpl(final BigDecimal amount) {
    this.amount = amount;
  }
  
  /**
   * Get the amount for this payment.
   * @return the amount
   */
  @Override
  public BigDecimal amount() {
    return amount;
  }
  
  /**
   * A factory-style equivalent to calling the constructor.
   * @param amount the amount for this payment
   * @return a Payment
   */
  public static PaymentImpl from(final BigDecimal amount) {
    return new PaymentImpl(amount);
  }
  
  /**
   * Copy this Payment, receiving a new instance.
   * @return a new instance of this payment with the same amount
   */
  public PaymentImpl copy() {
    return new PaymentImpl(this.amount);
  }
  
  @Override
  public String toString() {
    return "PaymentImpl{" + "amount=" + amount + '}';
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    PaymentImpl payment = (PaymentImpl) o;
    //this is important, as BigDecimal::equals can return false for floating-point after division
    return this.amount.compareTo(payment.amount) == 0;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(amount);
  }
}
