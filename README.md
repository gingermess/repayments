# Simple Repayments

The problem in a nutshell: given two command line arguments:

* An amount
* A number of repayments

calculate the amount of each repayment, adjusting the last to include any excess.

## Building

Execute the following from the project root:

`mvn clean package`

A shaded (uber) JAR will be placed into `target/`.

## Running

Simply `cd` to the location of the shaded JAR, and execute:

`java -jar repayments-1.0.jar <amount> <repayments>`

You'll see up to two values printed to stdout:

`Regular Amount �0.03`\
`Last Amount �0.04`

If `<amount>` is completely divisible by `<repayments>` the second value will not be printed.
Negative amounts are supported, but repayments must be at least 1. Too many payments for an amount will no doubt cause a horrible error, you have been warned!


